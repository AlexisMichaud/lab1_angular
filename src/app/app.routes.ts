//#region region MODULES
import { Routes } from "@angular/router";
//#endregion

//#region COMPONENTS
import { AppComponent } from "./components/app/app.component";
import { LoginComponent } from "./components/login/login.component";
import { LogoutComponent } from "./components/logout/logout.component";
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";
import { ProfileDetailComponent } from "./components/profile-detail/profile-detail.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { RecipeDetailComponent } from "./components/recipe-detail/recipe-detail.component";
import { RecipeNewComponent } from "./components/recipe-new/recipe-new.component";
import { RecipesComponent } from "./components/recipes/recipes.component";
import { SignupComponent } from "./components/signup/signup.component";
//#endregion

// GUARD
import { AuthGuard } from "./guards/auth.guard";
import { AuthedGuard } from "./guards/authed.guard";

export const ROUTES: Routes = [
	{ path: '', component: AppComponent },
	{
		path: '', canActivateChild: [AuthedGuard], children: [
			{ path: 'login', component: LoginComponent },
			{ path: 'signup', component: SignupComponent },
		]
	},
	{
		path: '', canActivateChild: [AuthGuard], children: [
			{ path: 'recipes', component: RecipesComponent },
			{ path: 'profile', component: ProfileComponent },
			{ path: 'recipe-new', component: RecipeNewComponent },
			{ path: 'recipe/:id', component: RecipeDetailComponent },
			{ path: 'profile/:email', component: ProfileDetailComponent }
		]
	},
	{ path: 'logout', component: LogoutComponent },
	{ path: '**', component: PageNotFoundComponent }
];
