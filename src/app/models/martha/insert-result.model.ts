import { JSONObject, optional, required } from 'ts-json-object'

export class InsertResult extends JSONObject {
	@required
	success!: boolean;

	@optional
	lastInsertId!: number;
}
