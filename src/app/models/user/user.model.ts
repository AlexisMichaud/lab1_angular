import { custom, JSONObject, required } from "ts-json-object";
import { Recipe } from "../recipes/recipe.model";

export class User extends JSONObject {
	@required
	@custom((user: User, key: string, value: string) => {
		return value.toLowerCase(); // normaliser les emails tous en minuscules (pour assurer unicité des users)
	})
	email!: string;
}
