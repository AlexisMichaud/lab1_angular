import { custom, JSONObject, required } from "ts-json-object";
import { Recipe } from "./recipe.model";

export class UserRecipes extends JSONObject {
	email!: string;

	recipes!: Recipe[];
}
