import { custom, JSONObject, required } from "ts-json-object";

export class Recipe extends JSONObject {
	@required
	name!: string;

	@required
	@custom((recipe: Recipe, key: string, value: number) => {
		return +value;
	})
	category!: number;

	@required
	description!: string;

	user_email!: string;

	@required
	id!: string;
}
