//#region MODULES
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from "@angular/forms";
import { AppRoutingModule } from './app-routing.module';
import { HttpClient, HttpClientModule } from "@angular/common/http";
//#endregion

//#region COMPONENTS
import { AppComponent } from './components/app/app.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { RecipesComponent } from './components/recipes/recipes.component';
import { ProfileComponent } from './components/profile/profile.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UsersListItemComponent } from './components/users-list-item/users-list-item.component';
import { LogoutComponent } from './components/logout/logout.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { RecipesListComponent } from './components/recipes-list/recipes-list.component';
import { RecipesListItemComponent } from './components/recipes-list-item/recipes-list-item.component';
import { RecipeNewComponent } from './components/recipe-new/recipe-new.component';
import { ProfileDetailComponent } from './components/profile-detail/profile-detail.component'
//#endregion

//#region CSS (FontAwesomeModule)
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RecipeDetailComponent } from './components/recipe-detail/recipe-detail.component';
import { ModalModule } from "ngx-bootstrap/modal";
import { UsersInfoModalComponent } from './modals/users-info-modal/users-info-modal.component';
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
//#endregion

export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http);
}

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		SignupComponent,
		RecipesComponent,
		ProfileComponent,
		UsersListComponent,
		UsersListItemComponent,
		LogoutComponent,
		PageNotFoundComponent,
		RecipesListComponent,
		RecipesListItemComponent,
		RecipeNewComponent,
		ProfileDetailComponent,
		RecipeDetailComponent,
		UsersInfoModalComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		ReactiveFormsModule,
		FontAwesomeModule,
		HttpClientModule,
		ModalModule.forRoot(),
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory, // exported factory function needed for AoT compilation
				deps: [HttpClient]
			}
		})
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
