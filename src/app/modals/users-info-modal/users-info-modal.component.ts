import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";

@Component({
	selector: 'app-users-info-modal',
	templateUrl: './users-info-modal.component.html',
	styleUrls: ['./users-info-modal.component.css']
})
export class UsersInfoModalComponent implements OnInit {
	modalRef!: BsModalRef;

	constructor(private modalService: BsModalService) { }

	ngOnInit(): void {
	}

	openModal(template: TemplateRef<any>) {
		this.modalRef = this.modalService.show(template);
	}
}
