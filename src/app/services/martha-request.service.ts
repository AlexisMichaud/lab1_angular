import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { InsertResult } from '../models/martha/insert-result.model';

@Injectable({
	providedIn: 'root'
})
export class MarthaRequestService {
	private readonly username = 'michaud';
	private readonly password = 'u4qji2oy';

	constructor(private http: HttpClient) { }

	private get headers() {
		return { headers: { 'auth': btoa(`${this.username}:${this.password}`) } };
	}

	private getUrl(query: string) {
		return `http://martha.jh.shawinigan.info/queries/${query}/execute`;
	}

	select(query: string, body: any = null): Observable<any> {
		return this.http.post<any>(this.getUrl(query), body, this.headers).pipe(
			tap(response => {
				console.log("Martha Request Service Select", response);
			}),
			map(response => {
				return response.success ? response.data : false;
			}),
			catchError(error => {
				return of(null);
			})
		);
	}

	insert(query: string, body: any = null): Observable<InsertResult | null> {
		return this.http.post<any>(this.getUrl(query), body, this.headers).pipe(
			tap(response => {
				console.log("Martha Request Service Insert", response);
			}),
			map(result => {
				return new InsertResult(result);
			}),
			catchError(error => {
				console.log('Error', error);

				return of(null);
			})
		);
	}
}
