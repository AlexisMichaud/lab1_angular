import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { map, tap } from "rxjs/operators";
import { InsertResult } from "../models/martha/insert-result.model";
import { Recipe } from "../models/recipes/recipe.model";
import { UserRecipes } from "../models/recipes/userRecipes.model";
import { User } from "../models/user/user.model";
import { AuthService } from "./auth.service";
import { MarthaRequestService } from "./martha-request.service";

@Injectable({
	providedIn: 'root'
})
export class RecipesService {
	private readonly CURRENT_RECIPE_KEY = 'recipeasy.currentRecipe';

	private _currentRecipe: Recipe | null = null;

	get currentRecipe(): Recipe | null {
		return this._currentRecipe;
	}

	private setCurrentRecipe(recipe: Recipe | null) {
		this._currentRecipe = recipe;
		localStorage.setItem(this.CURRENT_RECIPE_KEY, JSON.stringify(recipe));
	}

	constructor(private authService: AuthService, private martha: MarthaRequestService) {
		const storedCurrentRecipe = JSON.parse(localStorage.getItem(this.CURRENT_RECIPE_KEY) ?? 'null');

		if (storedCurrentRecipe) {
			this._currentRecipe = new Recipe(storedCurrentRecipe);
			console.log(this._currentRecipe);
		}
	}

	get recipes(): Observable<Recipe[]> {
		return this.martha.select("recipes-list", this.authService.currentUser).pipe(
			tap(response => {
				console.log("Recipe Service GetRecipes", response);
			}),
			map(data => {
				if (data && data.length >= 1) {
					return data;
				} else {
					return null;
				}
			})
		)
	}

	recipeDetail(recipe: Recipe): Observable<Recipe> {
		this.setCurrentRecipe(recipe);
		return this.martha.select("recipes-read", this.currentRecipe).pipe(
			tap(response => {
				console.log("Recipe Service GetRecipes", response);
			}),
			map(data => {
				if (data && data.length >= 1) {
					return data;
				} else {
					return null;
				}
			})
		)
	}

	updateRecipe(recipe: Recipe): Observable<InsertResult | null> {
		return this.martha.insert("recipes-update", recipe).pipe(
			tap(response => {
				console.log("Recipe Service GetRecipes", response);
			}),
			map(data => {
				if (data && data.success) {
					return data;
				} else {
					return null;
				}
			})
		)
	}

	newRecipe(recipe: Recipe): Observable<boolean> {
		recipe.user_email = this.authService.currentUser!.email;
		// console.log(recipe)
		// if (!recipe.description) {
		// 	console.log("SERVICE")
		// 	recipe.description = " ";
		// }

		return this.martha.insert("recipes-create", recipe).pipe(
			tap(response => {
				console.log("Recipe Service NewRecipe", response);
			}),
			map(data => {
				if (data?.success) {
					return true;
				} else {
					return false;
				}
			})
		)
	}

	deleteRecipe(recipe: Recipe): Observable<boolean> {
		return this.martha.insert("recipes-delete", recipe).pipe(
			tap(response => {
				console.log("Recipe Service DeleteRecipe", response);
			}),
			map(data => {
				if (data?.success) {
					return true;
				} else {
					return false;
				}
			})
		)
	}
}
