import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";

@Injectable({
	providedIn: 'root'
})
export class LanguageService {
	private readonly CURRENT_LANGUAGE_KEY = 'recipeasy.language';

	constructor(private translate: TranslateService, private router: Router) {
		this.translate.addLangs(['en', 'fr']);

		let storedLanguage = localStorage.getItem(this.CURRENT_LANGUAGE_KEY) ?? 'null';

		if (storedLanguage != 'null')
			this.translate.use(storedLanguage);
		else {
			this.translate.use('en');
			this.translate.setDefaultLang('en');
			localStorage.setItem(this.CURRENT_LANGUAGE_KEY, 'en');
		}
	}

	changeLanguage(lang: string) {
		if (lang != this.language) {
			this.translate.use(lang);
			this.translate.setDefaultLang(lang);
			localStorage.setItem(this.CURRENT_LANGUAGE_KEY, lang);
			window.location.reload();
		}
	}

	get language(): string {
		return localStorage.getItem(this.CURRENT_LANGUAGE_KEY)!;
	}
}
