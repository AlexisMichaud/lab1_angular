import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { catchError, map, tap } from "rxjs/operators";
import { UserCredentials } from "../models/user/user.credentials";
import { User } from "../models/user/user.model";
import { MarthaRequestService } from "./martha-request.service";
import { RecipesService } from "./recipes.service";

@Injectable({
	providedIn: 'root'
})
export class AuthService {
	private readonly CURRENT_USER_KEY = 'recipeasy.currentUser';

	private _currentUser: User | null = null;

	get currentUser(): User | null {
		return this._currentUser;
	}

	get users(): Observable<User[]> {
		return this.martha.select("users-list").pipe(
			tap(response => {
				console.log("Auth Service GetUsers", response);
			}),
			map(data => {
				if (data && data.length >= 1) {
					return data;
				} else
					return null;
			})
		)
	}

	get isLoggedIn(): boolean {
		console.log(!!this._currentUser);
		return !!this._currentUser;
	}

	constructor(private martha: MarthaRequestService) {
		const storedCurrentUser = JSON.parse(localStorage.getItem(this.CURRENT_USER_KEY) ?? 'null');

		if (storedCurrentUser) {
			this._currentUser = new User(storedCurrentUser);
		}
	}

	private setCurrentUser(user: User | null) {
		this._currentUser = user;
		localStorage.setItem(this.CURRENT_USER_KEY, JSON.stringify(user));
	}

	logIn(credentials: UserCredentials): Observable<boolean> {
		return this.martha.select("users-login", credentials).pipe(
			tap(response => {
				console.log("Auth Service Login", response);
			}),
			map(data => {
				if (data && data.length == 1) {
					this.setCurrentUser(new User(data[0]))
					return true;
				} else
					return false;
			})
		)
	}

	signUp(credentials: UserCredentials): Observable<boolean> {
		return this.martha.insert("users-signup", credentials).pipe(
			tap(response => {
				console.log("Auth Service Signup", response);
			}),
			map(data => {
				if (data?.success) {
					this.setCurrentUser(new User(credentials))
					return true;
				} else {
					return false;
				}
			})
		)
	}

	logOut() {
		this.setCurrentUser(null);
	}
}
