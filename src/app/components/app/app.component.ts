import { Component } from '@angular/core';
import { FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { AuthService } from "src/app/services/auth.service";
import { LanguageService } from "src/app/services/language.service";
import { RecipesService } from "src/app/services/recipes.service";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	languageForm: FormGroup;
	language: FormControl = new FormControl;
	title = 'Recipeasy';

	get isLogged() {
		return this.authService.isLoggedIn;
	}

	constructor(private authService: AuthService, private router: Router, private langService: LanguageService, private translate: TranslateService) {
		if (this.langService.language) {
			this.languageForm = new FormGroup({
				language: new FormControl(this.langService.language)
			});
			this.changeLanguage(this.langService.language);
		} else {
			this.languageForm = new FormGroup({
				language: new FormControl('en')
			});
		}
	}

	logOut() {
		this.authService.logOut();
		this.router.navigate(['/logout']);
	}

	changeLanguage(lang: string) {
		console.log("HAPPENING");
		this.langService.changeLanguage(lang);
		this.translate.use(lang.valueOf());
		console.log("CHANGE", lang);
	}
}
