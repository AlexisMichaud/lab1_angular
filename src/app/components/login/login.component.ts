import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { UserCredentials } from "src/app/models/user/user.credentials";
import { User } from "src/app/models/user/user.model";
import { AuthService } from "src/app/services/auth.service";
import { MarthaRequestService } from "src/app/services/martha-request.service";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	modalRef!: BsModalRef;
	loginForm: FormGroup;
	authError: string | null = null;
	showModalBox = false;

	// Ajout du mot-clé "private" plutôt que d'avoir à créer séparément la variable
	constructor(private authService: AuthService, private router: Router, private modalService: BsModalService) {
		// Déclaration d'un formgroup auquel on associe les formcontrolname
		this.loginForm = new FormGroup({
			email: new FormControl(null, [Validators.required, Validators.email]),
			password: new FormControl(null, [Validators.required, Validators.minLength(3)])
		});
	}

	ngOnInit(): void {
	}

	logIn() {
		this.authService.logIn(this.loginForm.value).subscribe(response => {
			console.log("Login Component", response);
			if (response)
				this.router.navigate(['/recipes']);
			else
				this.authError = "Invalid credentials!";
		});
		// console.log(this.loginForm.value);
	}

	open() {
		console.log("Opening")
		this.showModalBox = true;
	}
	openModal(template: TemplateRef<any>) {
		this.modalRef = this.modalService.show(template);
	}
}
