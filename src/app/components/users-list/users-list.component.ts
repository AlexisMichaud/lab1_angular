import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { User } from "src/app/models/user/user.model";
import { AuthService } from "src/app/services/auth.service";

@Component({
	selector: 'app-users-list',
	templateUrl: './users-list.component.html',
	styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
	users: User[] = [];

	constructor(private authService: AuthService, private router: Router) {
		this.getUsers();
	}

	ngOnInit(): void {
	}

	userClicked(u: User) {
		// Receives the entire user, u.email to navigate with only the user's email as param
		this.router.navigate(['/profile', u.email]);
	}

	private getUsers() {
		this.authService.users.subscribe(response => {
			console.log("Users List Component", response);
			this.users = response as User[];
		});
	}
}
