import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { Recipe } from "src/app/models/recipes/recipe.model";
import { AuthService } from "src/app/services/auth.service";
import { LanguageService } from "src/app/services/language.service";
import { RecipesService } from "src/app/services/recipes.service";

@Component({
	selector: 'app-recipes-list',
	templateUrl: './recipes-list.component.html',
	styleUrls: ['./recipes-list.component.css']
})
export class RecipesListComponent implements OnInit {
	title!: string;
	catTHeader!: string;
	nameTHeader!: string;
	descTHeader!: string;
	newBtn!: string;

	recipes: Recipe[] = [];

	constructor(private recipeService: RecipesService, private router: Router, private translate: TranslateService) {
		this.getRecipes();
	}

	ngOnInit(): void {
		this.translate.get(['recipesTitle', 'recipesTable.cat', 'recipesTable.name', 'recipesTable.desc', 'newRecipeBtn'])
			.subscribe(translations => {
				this.title = translations['recipesTitle'];
				this.catTHeader = translations['recipesTable.cat'];
				this.nameTHeader = translations['recipesTable.name'];
				this.descTHeader = translations['recipesTable.desc'];
				this.newBtn = translations['newRecipeBtn'];
			});
	}

	onClickDel(recipe: Recipe) {
		this.recipeService.deleteRecipe(recipe).subscribe(response => {
			console.log("Recipes List Component - DELETE", response);
			if (response) {
				this.getRecipes();
			}
			else {
				alert('Error while deleting the recipe.');
			}
		});
	}

	onClickEdit(recipe: Recipe) {
		this.recipeService.recipeDetail(recipe);
		this.router.navigate(['/recipe', recipe.id]);
		/*this.recipeService.updateRecipe(recipe);
		this.recipeService.updateRecipe(recipe).subscribe(response => {
			console.log("Recipes List Component", response);
			if (response) {
				this.getRecipes();
			}
			else {
				alert('Error while deleting the recipe.');
			}
		});*/
	}

	private getRecipes() {
		this.recipeService.recipes.subscribe(response => {
			console.log("Recipes List Component - GET", response);
			this.recipes = response as Recipe[];
		});
	}
}
