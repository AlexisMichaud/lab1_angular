import { Component, OnInit } from '@angular/core';
import { AuthService } from "src/app/services/auth.service";

@Component({
	selector: 'app-recipes',
	templateUrl: './recipes.component.html',
	styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {
	get currentUser() {
		return this.authService.currentUser;
	}

	constructor(private authService: AuthService) { }

	ngOnInit(): void {
	}

}
