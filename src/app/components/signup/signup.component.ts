import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { UserCredentials } from "src/app/models/user/user.credentials";
import { AuthService } from "src/app/services/auth.service";

@Component({
	selector: 'app-signup',
	templateUrl: './signup.component.html',
	styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
	signupForm: FormGroup;

	constructor(private authService: AuthService, private router: Router) {
		this.signupForm = new FormGroup({
			email: new FormControl(null, [Validators.required, Validators.email]),
			password: new FormControl(null, [Validators.required, Validators.minLength(3)]),
			pwdConfirm: new FormControl()
		}, this.passwordMatch);
	}

	ngOnInit(): void {
	}

	signUp() {
		this.authService.signUp(new UserCredentials(this.signupForm.value)).subscribe(response => {
			console.log("Signup Component", response);
			if (response) {
				this.router.navigate(['/recipes']);
			}
			else {
				alert('Email already exists.');
				console.log(this.signupForm.value);
			}
		});
	}

	private passwordMatch(form: AbstractControl): ValidationErrors | null {
		if (form.value?.password != form.value?.pwdConfirm)
			return { passwordConfirmationMustMatch: true };
		else
			return null
	}
}
