import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

@Component({
	selector: 'app-profile-detail',
	templateUrl: './profile-detail.component.html',
	styleUrls: ['./profile-detail.component.css']
})
export class ProfileDetailComponent implements OnInit {
	email!: string;

	constructor(private route: ActivatedRoute) {
		route = new ActivatedRoute;
	}

	ngOnInit(): void {
		this.email = this.route.snapshot.paramMap.get('email')!;
		console.log(this.email);
	}
}
