import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Recipe } from "src/app/models/recipes/recipe.model";

import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: '[app-recipes-list-item]',
	templateUrl: './recipes-list-item.component.html',
	styleUrls: ['./recipes-list-item.component.css']
})
export class RecipesListItemComponent implements OnInit {
	delBtn!: string;
	editBtn!: string;

	faTrash = faTrashAlt;
	faEdit = faEdit;

	@Input() recipe!: Recipe;
	@Output() recipeClicked = new EventEmitter();
	@Output() recipeEditClicked = new EventEmitter();

	constructor(private translate: TranslateService) {
	}

	ngOnInit(): void {
		this.translate.get(['delRecipeBtn', 'editRecipeBtn'])
			.subscribe(translations => {
				this.delBtn = translations['delRecipeBtn'];
				this.editBtn = translations['editRecipeBtn'];
			});
	}

	delClick() {
		this.recipeClicked.emit(this.recipe);
	}

	editClick() {
		this.recipeEditClicked.emit(this.recipe);
	}
}
