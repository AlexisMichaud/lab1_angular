import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Recipe } from "src/app/models/recipes/recipe.model";
import { AuthService } from "src/app/services/auth.service";
import { RecipesService } from "src/app/services/recipes.service";

@Component({
	selector: 'app-recipe-new',
	templateUrl: './recipe-new.component.html',
	styleUrls: ['./recipe-new.component.css']
})
export class RecipeNewComponent implements OnInit {
	recipeForm: FormGroup;
	category: FormControl = new FormControl;
	authError: string | null = null;

	get currentUser() {
		return this.authService.currentUser;
	}

	constructor(private recipeService: RecipesService, private router: Router, private authService: AuthService) {
		this.recipeForm = new FormGroup({
			description: new FormControl(""),
			name: new FormControl(null, [Validators.required]),
			category: new FormControl(-1, [Validators.required, Validators.min(0), Validators.max(2)])
		});
	}

	ngOnInit(): void {
	}

	newRecipe() {
		this.recipeForm.value.id = Math.random().toString(16).substring(2);

		this.recipeService.newRecipe(new Recipe(this.recipeForm.value)).subscribe(response => {
			console.log("Recipe New Component", response);
			if (response) {
				this.router.navigate(['/recipes']);
			}
			else {
				this.authError = "Be sure to fill all fields.";
			}
		});
	}
}
