import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Recipe } from "src/app/models/recipes/recipe.model";
import { RecipesService } from "src/app/services/recipes.service";

@Component({
	selector: 'app-recipe-detail',
	templateUrl: './recipe-detail.component.html',
	styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
	recipe_id!: string;
	recipeEditForm: FormGroup;
	category: FormControl = new FormControl;
	authError: string | null = null;

	get currentRecipe() {
		return this.recipeService.currentRecipe;
	}

	constructor(private route: ActivatedRoute, private recipeService: RecipesService, private router: Router) {
		console.log(this.currentRecipe);
		this.getCurrentRecipe();

		route = new ActivatedRoute;
		this.recipeEditForm = new FormGroup({
			description: new FormControl(this.currentRecipe?.description),
			name: new FormControl(this.currentRecipe?.name, [Validators.required]),
			category: new FormControl(this.currentRecipe?.category, [Validators.required, Validators.min(0), Validators.max(2)])
		});
	}

	ngOnInit(): void {
		this.recipe_id = this.route.snapshot.paramMap.get('id')!;
		console.log(this.recipe_id);
	}


	updateRecipe() {
		this.recipeService.updateRecipe(new Recipe({
			name: this.recipeEditForm.get('name')?.value,
			description: this.recipeEditForm.get('description')!.value,
			category: this.recipeEditForm.get('category')!.value,
			id: this.recipe_id
		}))
			.subscribe(response => {
				console.log("Recipes List Component", response);
				if (response) {
					this.router.navigate(['/recipes']);
				}
				else {
					this.authError = "Be sure to fill all fields.";
				}
			});
	}

	private getCurrentRecipe() {
		this.recipeService.recipeDetail(this.recipeService.currentRecipe!).subscribe(response => {
			console.log("Recipes List Component - GET", response);
		});
	}
}
